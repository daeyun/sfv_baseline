import matplotlib as mpl

mpl.use('Agg')

import numpy as np
import scipy.linalg as la
import h5py
from os import path
import matplotlib.pyplot as pt
import sys
import sfv
from dshin import geom3d
from dshin import geom2d
from dshin import osmesa_renderer
import sklearn.mixture as sklearn_mixture
import matplotlib.cm as cm
from skimage import color as skcolor
import maxflow
from scipy import ndimage

from sfv import pairwise
from sfv import unary
import sfv.voxels
from sfv import camera
from sfv import utils
from sfv import import_data
import hashlib
import pickle


def run(cameras, images, phi=0.65, resolution=60, gmm_samples_per_image=1200):
    bbox = sfv.utils.initial_bounding_box(cameras, visualize=False)
    vg = sfv.voxels.VoxelGrid(bbox, resolution=resolution)
    graph = maxflow.GraphFloat()
    nodeids = graph.add_grid_nodes(vg.shape)

    print("Adding unaries")
    sfv.unary.add_initial_unary_costs_to_graph(
            images, cameras, graph, vg,
            gmm_samples_per_image=gmm_samples_per_image, phi=phi)

    print("Adding pairwise")
    graph.add_grid_edges(nodeids, 0.1)
    sfv.pairwise.add_pairwise_costs_to_graph(images, cameras, graph, vg)

    graph.maxflow()
    seg3d = graph.get_grid_segments(nodeids).astype(np.float32)


if __name__ == '__main__':
    data = import_data.import_mat_file()
    images = data[0]['images']
    cameras = data[0]['cameras']
    phi = 0.6
    resolution = 100

    run(cameras, images, phi=phi, resolution=resolution,
        gmm_samples_per_image=1200)
