import matplotlib as mpl
mpl.use('Agg')

import numpy as np
import scipy.linalg as la
import h5py
from os import path
import matplotlib.pyplot as pt
import sys
import sfv
from dshin import geom3d
from dshin import geom2d
from dshin import osmesa_renderer
import sklearn.mixture as sklearn_mixture
import matplotlib.cm as cm
from skimage import color as skcolor
import maxflow
from scipy import ndimage

import sfv.voxels
from sfv import camera
from sfv import utils
from sfv import import_data
import hashlib
import pickle

cache_path = path.expanduser('~/Dropbox/data/sfv_cache/')


class NoVolumeException(Exception):
    pass


def run(cameras, images, phi=0.65, resolution=60, gmm_samples_per_image=1200,
        visualize=False, cache_name=None):
    """
    Raises NoVolumeException in case of failure.
    """
    # cameras = data[i]['cameras']
    # images = data[i]['images']

    pickle_name = ''
    if cache_name is not None:
        md5 = hashlib.md5()
        md5.update(cache_name.encode('utf-8'))
        hsh = md5.hexdigest()
        pickle_name = 'volumeonly_{}.pickle'.format(hsh[:10])
        pickle_name = path.join(cache_path, pickle_name)
        if path.isfile(pickle_name):
            print('loading from {}'.format(pickle_name))
            with open(pickle_name, 'rb') as fp:
                data = pickle.load(fp)
            return data

    bbox = sfv.utils.initial_bounding_box(cameras, visualize=False)

    vg = sfv.voxels.VoxelGrid(bbox, resolution=resolution)

    bg_px = sfv.utils.initial_background_pixels(
            images, gmm_samples_per_image)
    fg_px = sfv.utils.initial_foreground_pixels(
            images, cameras, gmm_samples_per_image)
    bg_model, fg_model = sfv.utils.learn_color_models(bg_px, fg_px)

    fgs = []
    for i in range(images.shape[0]):
        fg = sfv.utils.apply_color_models(images[i], bg_model, fg_model)
        fgs.append(fg)

    # sfv.utils.display_montage(np.concatenate([fg[None, :, :] for fg in fgs], axis=0))

    Gfg = vg.zero_grid()
    for i in range(len(cameras)):
        xy, ids = vg.project_to_image(cameras[i], images[i].shape)
        Gfg[vg.linear_to_multi(ids)] += fgs[i][xy[:, 1], xy[:, 0]]
    Gfg /= len(cameras)

    graph = maxflow.Graph[float]()

    nodeids = graph.add_grid_nodes(vg.shape)

    Gfg[[0, -1], :, :] = -2
    Gfg[:, [0, -1], :] = -2
    Gfg[:, :, [0, -1]] = -2

    graph.add_grid_tedges(nodeids, 1 - (Gfg - phi), 1 + (Gfg - phi))

    graph.add_grid_edges(nodeids, 0.01)

    graph.maxflow()

    seg3d = graph.get_grid_segments(nodeids).astype(np.float32)

    if seg3d.max() - seg3d.min() == 0:
        raise NoVolumeException("failed: phi {}".format(phi))

    mesh = sfv.voxels.voxel_grid_to_mesh(seg3d, bbox=bbox, crossing=0.5)
    # sfv.utils.open_in_meshlab(mesh['v'], mesh['f'])

    mesh_projections = []
    proj_visualizations = []
    for i in range(len(cameras)):
        P = cameras[i].projection_mat34()
        tri = mesh['v'][mesh['f'].ravel()]
        im_wh = images[0].shape[:2][::-1]
        silh = osmesa_renderer.render_silhouette(tri, P, out_wh=im_wh, lrtb=(
            0, im_wh[0] - 1, 0, im_wh[1] - 1))
        if visualize:
            proj_visualizations.append(
                    np.dstack((fgs[i], silh, np.zeros(silh.shape))))
        mesh_projections.append(silh)

    if visualize:
        sfv.utils.display_montage(proj_visualizations)

        # from skimage.filters import sobel as skisobel
        # silhouette_montage = sfv.utils.montage(silhouetttes, 8)
        # fg_montage = sfv.utils.montage(fgs, 8)
        # silhouette_edge = skisobel(silhouette_montage) > 0
        # silhouette_edge = ndimage.binary_dilation(silhouette_edge, iterations=7)
        # pt.figure(figsize=(16, 12))

        #     seg_montage = np.tile(fg_montage[:,:,None], (1,1,3))
        #     seg_montage[:,:,0][silhouette_edge] = 1
        #     seg_montage[:,:,1][silhouette_edge] = 0
        #     seg_montage[:,:,2][silhouette_edge] = 0
        #     pt.imshow(seg_montage)
        pt.show()

    return_values = {
        'mesh': mesh,
        'vg': vg,
        'seg3d': seg3d,
    }

    if cache_name is not None:
        print('saving {}'.format(pickle_name))
        with open(pickle_name, 'wb') as f:
            pickle.dump(return_values, f, pickle.HIGHEST_PROTOCOL)
        with open(path.join(path.dirname(pickle_name), 'index.txt'), 'a') as f:
            f.write('{}  {}\n'.format(cache_name, pickle_name))

    return return_values


if __name__ == '__main__':
    data = sfv.import_data.import_mat_file('~/Documents/SFV_baseline/')
    for i in range(3):
        cameras = data[i]['cameras']
        images = data[i]['images']
        import time

        start = time.time()
        result = run(cameras, images, phi=0.65, resolution=50,
                     gmm_samples_per_image=1000, visualize=False)
        print("Elapsed time: ", time.time() - start)
