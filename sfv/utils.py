import matplotlib.cm as cm
import matplotlib.pyplot as pt
import maxflow
import numpy as np
from sklearn import mixture as skmixture

import sfv
from sfv import camera
import hashlib


def pick_border_pixels(images, npick):
    if len(images.shape) == 3:
        images = images[None, :]
    mask = sfv.utils.image_border_mask(images[0], padding=0.1)
    return pick_masked_pixels(images, mask, npick=npick)


def pick_masked_pixels(images: np.ndarray, mask, npick=10000):
    if len(images.shape) == 3:
        images = images[None, :]
    assert len(mask.shape) > 1
    assert len(images.shape) == 4
    subs = np.vstack(np.where(mask))
    chosen = subs[:, np.random.choice(subs.shape[1], npick)]
    pixels = images[:, chosen[0, :], chosen[1, :], :]
    return pixels.reshape((pixels.shape[0] * pixels.shape[1], -1))


def random_choice_from_mask(mask, n):
    subs = np.vstack(np.where(mask))
    selected_inds = np.random.choice(subs.shape[1], n)
    return subs[:, selected_inds]


def image_border_mask(im, padding=None):
    mask = np.ones(im.shape[:2], dtype=np.bool)
    if padding is None:
        padding = int(np.min(im.shape[:2]) * 0.1)
    elif padding < 1:
        padding = int(np.min(im.shape[:2]) * padding)
    mask[padding:-padding, padding:-padding] = False
    return mask


def mask_near_xy(im, xy, radius=None):
    mask = np.zeros(im.shape[:2], dtype=np.bool)
    if radius is None:
        radius = int(np.min(im.shape[:2]) * 0.2)
    elif radius < 1:
        radius = int(np.min(im.shape[:2]) * radius)
    y, x = np.round(xy).astype(np.int)
    mask[max(0, y - radius):min(im.shape[0], y + radius),
    max(0, x - radius):min(im.shape[0], x + radius)] = True
    return mask


def pick_pixels_near_xy(image, xy, npick, radius=0.2):
    n1 = mask_near_xy(image, xy, radius / 3)
    n2 = mask_near_xy(image, xy, radius)

    pixels = np.vstack(
            (pick_masked_pixels(image, n1, int(npick * (1 / 3.0) + 0.5)),
             pick_masked_pixels(image, n2, int(npick * (2 / 3.0) + 0.5))))
    return pixels


def initial_foreground_pixels(images, cameras, npick):
    fixation_xyz = sfv.camera.camera_fixation_centroid(cameras)
    fg_pixels = []
    for i in range(len(cameras)):
        imxy = cameras[i].world_to_image(fixation_xyz[None, :]).ravel()
        npick_i = int(npick / (len(cameras) - i) + 0.5)
        fg_pixels.append(
                sfv.utils.pick_pixels_near_xy(images[i], imxy, npick_i))
        npick -= npick_i
    fg_pixels = np.vstack(fg_pixels)
    return fg_pixels


def initial_background_pixels(images, npick):
    return sfv.utils.pick_border_pixels(images, npick)


def montage(images, gridwidth=None):
    if gridwidth is None:
        gridwidth = int(np.ceil(np.sqrt(len(images))))
    gridheight = int(np.ceil(len(images) / gridwidth))
    remaining = gridwidth * gridheight
    rows = []
    while remaining > 0:
        rowimgs = images[:gridwidth]
        images = images[gridwidth:]
        nblank = gridwidth - len(rowimgs)
        rowimgs.extend([np.zeros(rowimgs[0].shape)] * nblank)
        remaining -= gridwidth
        row = np.hstack(rowimgs)
        rows.append(row)
    m = np.vstack(rows)
    return m


def learn_color_models(bg_px, fg_px):
    bg_model = skmixture.GMM(n_components=5, covariance_type='full', n_init=1,
                             n_iter=300, tol=0.0001, min_covar=0.0001).fit(
            bg_px)
    fg_model = skmixture.GMM(n_components=5, covariance_type='full', n_init=1,
                             n_iter=300, tol=0.0001, min_covar=0.0001).fit(
            fg_px)
    return bg_model, fg_model


def apply_color_models(image, bg_model, fg_model):
    impts = image.reshape(image.shape[0] * image.shape[1], 3)
    l_fg = np.exp(fg_model.score(impts).reshape(image.shape[:2]))
    l_bg = np.exp(bg_model.score(impts).reshape(image.shape[:2]))
    fg = l_fg / (l_fg + l_bg + 0.000001)
    # bg = l_bg / (l_fg + l_bg + 0.000001)
    return fg


def visualize_prob_map(l):
    pt.imshow(l, cmap=cm.get_cmap('gray'))
    pt.clim(l.min(), l.max())
    pt.colorbar()
    pt.title('Foreground likelihood')


def image_likelihood_sample(images, bg_model, fg_model):
    inds = range(0, len(images), 4)
    ls = []
    for i in inds:
        l = apply_color_models(images[i], bg_model, fg_model)[0]
        ls.append(l)
    im = np.hstack(ls)
    return im


def add_2d_graph_nodes(graph, images, bg_model, fg_model, visualize=False):
    # add nodes
    nodeids = graph.add_grid_nodes(images.shape[:3])

    pairwise_bias = 0.5
    pairwise_bias2 = 1.5

    # add pairwise
    w = np.sqrt(np.power((images[:, 1:, :, :] -
                          images[:, :-1, :, :]), 2).sum(axis=3))

    w = np.concatenate((w, np.ones((w.shape[0], 1, w.shape[2]))), axis=1)
    structure = np.array([[0, 0, 0],
                          [0, 0, 0],
                          [0, 1, 0]])
    graph.add_grid_edges(nodeids, weights=pairwise_bias2 * w + pairwise_bias,
                         structure=structure, symmetric=True)

    w = np.sqrt(np.power((images[:, :, 1:, :] -
                          images[:, :, :-1, :]), 2).sum(axis=3))
    w = np.concatenate((w, np.ones((w.shape[0], w.shape[1], 1))), axis=2)
    structure = np.array([[0, 0, 0],
                          [0, 0, 1],
                          [0, 0, 0]])
    graph.add_grid_edges(nodeids, weights=pairwise_bias2 * w + pairwise_bias,
                         structure=structure, symmetric=True)

    w = np.sqrt(np.power((images[:, 1:, 1:, :] -
                          images[:, :-1, :-1, :]), 2).sum(axis=3))
    w = np.concatenate((w, np.ones((w.shape[0], w.shape[1], 1))), axis=2)
    w = np.concatenate((w, np.ones((w.shape[0], 1, w.shape[2]))), axis=1)
    structure = np.array([[0, 0, 0],
                          [0, 0, 0],
                          [0, 0, 1 / np.sqrt(2)]])
    graph.add_grid_edges(nodeids, weights=pairwise_bias2 * w + pairwise_bias,
                         structure=structure, symmetric=True)

    w = np.sqrt(np.power((images[:, 1:, :-1, :] -
                          images[:, :-1, 1:, :]), 2).sum(axis=3))
    w = np.concatenate((np.ones((w.shape[0], w.shape[1], 1)), w), axis=2)
    w = np.concatenate((w, np.ones((w.shape[0], 1, w.shape[2]))), axis=1)
    structure = np.array([[0, 0, 0],
                          [0, 0, 0],
                          [1 / np.sqrt(2), 0, 0]])
    graph.add_grid_edges(nodeids, weights=pairwise_bias2 * w + pairwise_bias,
                         structure=structure, symmetric=True)

    # add unary
    fgs = []
    for i in range(images.shape[0]):
        fg, bg = apply_color_models(images[i], bg_model, fg_model)
        fgs.append(fg)
        graph.add_grid_tedges(nodeids[i], bg, fg)

    if visualize:
        display_montage(np.concatenate([fg[None, :, :] for fg in fgs], axis=0))

    return nodeids


def seg_2d(images, cameras, lab_images=None, visualize=False):
    bg_px = sfv.utils.initial_background_pixels(images, 2000)
    fg_px = sfv.utils.initial_foreground_pixels(images, cameras, 2000)
    bg_model, fg_model = sfv.utils.learn_color_models(bg_px, fg_px)

    g = maxflow.Graph[float]()
    nodeids = sfv.utils.add_2d_graph_nodes(
            g, images if lab_images is None else lab_images, bg_model, fg_model,
            visualize=visualize)

    g.maxflow()
    grid_seg = g.get_grid_segments(nodeids)

    if visualize:
        display_montage(grid_seg)

    return grid_seg


def display_montage(images):
    if type(images) is not list:
        images = [images[i] for i in range(images.shape[0])]

    m = sfv.utils.montage(images, 8)
    pt.imshow(m, cmap=cm.get_cmap('gray'))
    pt.clim(m.min(), m.max())
    pt.title("min: {}, max: {}".format(m.min(), m.max()))


def plot_mesh(verts, faces):
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d.art3d import Poly3DCollection

    fig = plt.figure(figsize=(12, 12))
    ax = fig.add_subplot(111, projection='3d')

    mesh = Poly3DCollection(verts[faces])
    ax.add_collection3d(mesh)

    bmax = verts.max(axis=0)
    bmin = verts.min(axis=0)
    padding = (bmax - bmin) / 10
    bmax += padding
    bmin -= padding

    ax.set_xlim(bmin[0], bmax[0])
    ax.set_ylim(bmin[1], bmax[1])
    ax.set_zlim(bmin[2], bmax[2])

    plt.show()


def open_in_meshlab(verts, faces):
    import tempfile
    import os
    with tempfile.NamedTemporaryFile(prefix='mesh_', suffix='.off',
                                     delete=False) as fp:
        fp.write('OFF\n{} {} 0\n'.format(verts.shape[0], faces.shape[0]).encode(
                'utf-8'))
        np.savetxt(fp, verts, fmt='%.5f')
        np.savetxt(fp, np.hstack((3 * np.ones((faces.shape[0], 1)), faces)),
                   fmt='%d')
        fname = fp.name
    os.system(
            'while [ ! -f {fname} ]; do sleep 0.5; done; meshlab {fname}'.format(
                    fname=fname))


def initial_bounding_box(cameras, visualize=False):
    ncams = len(cameras)
    positions = []
    for i in range(ncams):
        positions.append(cameras[i].position())
    s = (np.vstack(positions).max(axis=0) - np.vstack(positions).min(
            axis=0)).max() / 2
    c = sfv.camera.camera_fixation_centroid(cameras)
    bbox = np.vstack((c - s, c + s))

    if visualize:
        from dshin import geom3d
        fig = pt.figure(figsize=(8, 8))
        ax = fig.add_subplot(111, projection='3d', aspect='equal')
        geom3d.draw_cameras(cameras, ax)
        geom3d.pts(c[None, :], ax=ax, markersize=100)
        geom3d.cube(c, radius=s, ax=ax)

    return bbox


from os import path


def read_mesh(filename):
    """
    :param filename: full path to mesh file.
    :return: dict with keys v and f.
        v: ndarray of size (num_vertices, 3), type uint32. zero-indexed.
        f: ndarray of size (num_faces, 3), type float64.
    """
    if filename.endswith('.off'):
        return read_off(filename)


def read_off(filename):
    filename = path.expanduser(filename)
    with open(filename) as f:
        content = f.read()
    lines = content.splitlines()

    assert lines[0].upper() == 'OFF'
    num_vertices, num_faces, _ = [int(val) for val in lines[1].split(' ')]

    vertices = np.fromstring(' '.join(lines[2:num_vertices + 2]),
                             dtype=np.float64, sep=' ').reshape((-1, 3))
    faces = np.fromstring(
            ' '.join(lines[num_vertices + 2:num_vertices + num_faces + 2]),
            dtype=np.uint32,
            sep=' ').reshape((-1, 4))

    assert len(lines) == num_vertices + num_faces + 2
    assert (faces[:, 0] == 3).all(), "all triangle faces"

    faces = faces[:, 1:]

    if faces.min() != 0:
        print('faces.min() != 0')

    if faces.max() != vertices.shape[0] - 1:
        print('faces.max() != vertices.shape[0]-1')
        assert faces.max() < vertices.shape[0]

    return {
        'v': vertices,
        'f': faces,
    }


def md5(s):
    md5 = hashlib.md5()
    md5.update(s.encode('utf-8'))
    return md5.hexdigest()


def visualize_segmentation(seg3d, images, cameras, bbox):
    mesh = sfv.voxels.voxel_grid_to_mesh(seg3d, bbox=bbox, crossing=0.5)
    visualize_mesh(mesh, images, cameras)


def visualize_volume(vol, num=20):
    import matplotlib.pyplot as pt
    import matplotlib.cm as cm
    inds = np.linspace(0, vol.shape[0] - 1, num)
    slices = []
    for i in inds:
        volim = vol[i, :, :]
        slices.append(volim)
    sfv.utils.display_montage(slices)
    pt.colorbar()


def save_off_mesh(mesh, filename):
    verts = mesh['v'].astype(np.float32)
    faces = mesh['f'].astype(np.int32)
    with open(path.expanduser(filename), 'ab') as fp:
        fp.write('OFF\n{} {} 0\n'.format(verts.shape[0], faces.shape[0]).encode('utf-8'))
        np.savetxt(fp, verts, fmt='%.5f')
        np.savetxt(fp, np.hstack((3 * np.ones((faces.shape[0], 1)), faces)), fmt='%d')
