import numpy as np
import scipy.linalg as la


class Camera(object):
    def __init__(self, R: np.ndarray, t: np.ndarray, K: np.ndarray = None, s: float = 1.0, is_world_to_cam=True):
        assert R.shape == (3, 3)
        assert K.shape == (3, 3)
        if len(t.shape) == 1:
            t = t[None, :]
        assert t.shape == (3, 1)

        self._check_orthogonal(R)

        self.K = K
        self.K_inv = la.inv(K)

        # self.{R, t, s} will always be world_to_cam regardless of initialization.
        if is_world_to_cam:
            self.R, self.t, self.s = R, t, s
            self.R_inv, self.t_inv, self.s_inv = self.inverse(R, t, s)
        else:
            self.R_inv, self.t_inv, self.s_inv = R, t, s
            self.R, self.t, self.s = self.inverse(R, t, s)

    def _check_orthogonal(self, R, eps=1e-4):
        assert la.norm(R.T - la.inv(R), 2) < eps

    def inverse(self, R: np.ndarray, t: np.ndarray, s: float = 1):
        self._check_orthogonal(R)
        if len(t.shape) == 1:
            t = t[None, :]

        R_inv = R.T
        t_inv = -R.T.dot(t) / s
        s_inv = 1.0 / s

        return R_inv, t_inv, s_inv

    def _hom(self, pts):
        assert pts.shape[1] in [2, 3]
        return np.hstack((pts, np.ones((pts.shape[0], 1))))

    def _hom_inv(self, pts):
        assert pts.shape[1] in [3, 4]
        return pts[:, :-1] / pts[:, -1, None]

    def position(self):
        return (-self.R.T.dot(self.t) / self.s).ravel()

    def image_to_world(self, xys):
        if xys.shape[1] == 2:
            xys = self._hom(xys)
        return self.cam_to_world(self.K_inv.dot(xys.T).T)

    def cam_to_world(self, xyzs):
        sRt_inv = np.hstack((self.s_inv * self.R_inv, self.t_inv))
        xyzs = self._hom(xyzs)
        return sRt_inv.dot(xyzs.T).T

    def world_to_cam(self, xyzs):
        sRt = np.hstack((self.s * self.R, self.t))
        return sRt.dot(self._hom(xyzs).T).T

    def world_to_image(self, xyzs):
        # return self._hom_inv(self.K.dot(self.world_to_cam(xyzs).T).T)
        P = self.projection_mat34()
        return self._hom_inv(P.dot(self._hom(xyzs).T).T)

    def projection_mat34(self):
        return self.K.dot(np.hstack((self.s*self.R,  self.t)))


def camera_fixation_centroid(cameras):
    lines = []
    for cam in cameras:
        pos = cam.position()
        imcenter = cam.image_to_world(np.array([[cam.K[0, 2], cam.K[1, 2], 1]]))
        v = (pos - imcenter)
        lines.append(np.hstack((pos[None, :], v / la.norm(v, 2))))

    A = []
    b = []
    for line in lines:
        A.append([[line[0, 4], -line[0, 3], 0], [line[0, 5], 0, -line[0, 3]]])
        b.extend([la.det(np.vstack((line[0, (0, 1)], line[0, (3, 4)]))),
                  la.det(np.vstack((line[0, (0, 2)], line[0, (3, 5)])))])
    A = np.vstack(A)
    b = np.array(b)[:, None]
    x = la.lstsq(A, b)[0].ravel()
    return x
