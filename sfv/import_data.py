import numpy as np
import h5py
from os import path
import sfv
from sfv import camera
from sfv import utils
import pickle

cache_dir = path.expanduser('~/Dropbox/data/sfv_cache/')


def import_mat_file(dirpath="~/Dropbox/sfv_data/", use_cache=True):
    names = ['books', 'dragon', 'helmet', 'godzilla', 'hedgehog',
             'keyboard', 'paper_punch', 'rubik_cube', 'steplar', 'teabox',
             'godzilla_floor', 'hedgehog_floor', ]
    dirpath = path.expanduser(dirpath)
    hsh = utils.md5(",".join(names))
    pickle_name = path.join(cache_dir, 'sfv_cache_' + hsh[:10] + '.pickle')

    if use_cache and path.isfile(pickle_name):
        print('loading from {}'.format(pickle_name))
        with open(pickle_name, 'rb') as fp:
            data = pickle.load(fp)
        return data

    data = []
    for name in names:
        print(name, end=' ', flush=True)

        matfile = path.join(dirpath, '{}.mat'.format(name))
        mat = h5py.File(matfile, 'r')

        mat.get('cameras')

        cameras = []
        for i in range(mat.get('cameras')['R'].shape[1]):
            R = np.array(mat[mat.get('cameras')['R'][0, i]]).T
            t = np.array(mat[mat.get('cameras')['t'][0, i]]).T
            K = np.array(mat[mat.get('cameras')['K'][0, i]]).T
            s = np.array(mat[mat.get('cameras')['s'][0, i]])[0, 0]
            cam = sfv.camera.Camera(R, t, K, s, is_world_to_cam=False)
            cameras.append(cam)

        images = np.concatenate(
                [np.array(mat[mat.get('images')[0, i]]).transpose([2, 1, 0])[
                 None, :] for i in range(mat.get('images').shape[1])], axis=0)
        data.append({
            'cameras': cameras,
            'images': images.astype(np.float32) / 255.0,
        })
    print()

    print('saving {}'.format(pickle_name))
    with open(pickle_name, 'wb') as f:
        pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)

    return data


if __name__ == '__main__':
    import_mat_file()
