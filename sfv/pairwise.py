import numpy as np
from sfv import voxels
from sfv import camera
from sfv import import_data
import sfv
import maxflow


def pairwise_costs(image: np.ndarray, cam: camera.Camera, vg: voxels.VoxelGrid):
    imwh = image.shape[:2][::-1]
    xy, ids = vg.project_to_image(cam, imwh[::-1])

    i1, i2, i3 = vg.linear_to_multi(ids)

    vox = np.tile(vg.zero_grid()[:, :, :, None], (1, 1, 1, 3))
    vox[:] = np.nan
    vox[i1, i2, i3, :] = image[xy[:, 1], xy[:, 0], :]

    diff = np.hstack((np.power((vox[:-1, :, :, :] - vox[1:, :, :, :]), 2
                               ).sum(axis=3).ravel(),
                      np.power((vox[:, :-1, :, :] - vox[:, 1:, :, :]), 2
                               ).sum(axis=3).ravel(),
                      np.power((vox[:, :, :-1, :] - vox[:, :, 1:, :]), 2
                               ).sum(axis=3).ravel()))

    lig = vg.linear_index_grid()
    ids = np.hstack((np.vstack((lig[:-1, :, :].ravel(), lig[1:, :, :].ravel())),
                     np.vstack((lig[:, :-1, :].ravel(), lig[:, 1:, :].ravel())),
                     np.vstack(
                             (lig[:, :, :-1].ravel(), lig[:, :, 1:].ravel()))))

    valid = ~np.isnan(diff)

    ids = ids[:, valid].T
    diff = diff[valid]

    return ids.astype(np.uint32), diff


def pairwise_costs2(image: np.ndarray, cam: camera.Camera,
                    vg: voxels.VoxelGrid):
    imwh = image.shape[:2][::-1]
    xy, ids = vg.project_to_image(cam, imwh[::-1])

    projmap = np.zeros(imwh[::-1])
    projmap[:] = np.nan
    projmap[xy[:, 1], xy[:, 0]] = ids

    im_pairs = np.vstack((
        np.hstack((projmap[:-1, :].ravel()[:, None],
                   projmap[1:, :].ravel()[:, None])),
        np.hstack((projmap[:, :-1].ravel()[:, None],
                   projmap[:, 1:].ravel()[:, None]))))

    valid2d = ~np.isnan(im_pairs).any(1)
    im_pairs = im_pairs[valid2d, :]

    im_pairs = np.sort(im_pairs, axis=1)

    z_pairs = (im_pairs[:, 1] - im_pairs[:, 0]) == 1
    y_pairs = (im_pairs[:, 1] - im_pairs[:, 0]) == vg.shape[2]
    x_pairs = (im_pairs[:, 1] - im_pairs[:, 0]) == vg.shape[1] * vg.shape[2]

    valid_3d = x_pairs | y_pairs | z_pairs

    pair_ids = im_pairs[valid_3d]

    valid = valid2d.nonzero()[0][valid_3d]

    pair_diffs = np.power(
            np.vstack((image[:-1, :, :].reshape((-1, 3)),
                       image[:, :-1, :].reshape((-1, 3))))[valid, :] -
            np.vstack((image[1:, :, :].reshape((-1, 3)),
                       image[:, 1:, :].reshape((-1, 3))))[valid, :]
            , 2).sum(1)

    return pair_ids.astype(np.uint32), pair_diffs


def add_pairwise_costs_to_graph(images, cameras, graph, vg, weight=1):
    cost_map = {}

    for i in range(len(images)):
        image = images[i]
        cam = cameras[i]
        pair_ids, pair_diffs = pairwise_costs(image, cam, vg)

        beta = 1.0 / (2 * np.hstack((
            np.power((image[:, 1:, :] - image[:, :-1, :]), 2).sum(2).ravel(),
            np.power((image[1:, :, :] - image[:-1, :, :]), 2).sum(2).ravel()
        )).mean())

        costs = np.exp(-beta * pair_diffs)

        for ids, c in zip(pair_ids, costs):
            key = (ids[0], ids[1])
            if key in cost_map:
                cost_map[key] = min(c, cost_map[key])
            else:
                cost_map[key] = c

    lig = vg.linear_index_grid()
    all_pairs = np.hstack(
            (np.vstack((lig[:-1, :, :].ravel(), lig[1:, :, :].ravel())),
             np.vstack((lig[:, :-1, :].ravel(), lig[:, 1:, :].ravel())),
             np.vstack((lig[:, :, :-1].ravel(), lig[:, :, 1:].ravel())))).T
    null_pairs = set((p[0], p[1]) for p in all_pairs) - set(cost_map.keys())

    k = np.array(list(cost_map.keys()))
    v = np.array(list(cost_map.values()))
    for ids, val in zip(k, v):
        graph.add_edge(ids[0], ids[1], weight * val, weight * val)
    # print("max: {}, min: {}, mean: {}".format(weight * v.max(), weight * v.min(), weight * v.mean()))

    for ids in null_pairs:
        graph.add_edge(ids[0], ids[1], weight, weight)

        # from sfv import utils
        # vizvol = vg.zero_grid()
        # vizvol[:] = v.max()
        # i1, i2, i3 = vg.linear_to_multi(k[:, 0])
        # vizvol[i1, i2, i3] = v
        # utils.visualize_volume(vizvol, 40)


if __name__ == '__main__':
    data = import_data.import_mat_file()
    images = data[0]['images']
    cameras = data[0]['cameras']
    resolution = 200

    bbox = sfv.utils.initial_bounding_box(cameras, visualize=False)
    vg = sfv.voxels.VoxelGrid(bbox, resolution=resolution)

    graph = maxflow.GraphFloat()
    graph.add_grid_nodes(vg.shape)
    add_pairwise_costs_to_graph(images, cameras, graph, vg)
