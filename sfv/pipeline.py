#!/usr/bin/env python3

import matplotlib as mpl

mpl.use('Agg')
import matplotlib.pyplot as pt

import numpy as np
import sfv
from dshin import osmesa_renderer
import matplotlib.cm as cm
import maxflow

import sfv.voxels
import time
from sfv import utils
from sfv import unary
from sfv import pairwise
from sfv import import_data
from os import path
import os
import pickle


class NoVolumeException(Exception):
    pass


def estimate_boundary(images, cameras, bbox, gmm_samples=2000, resolution=60,
                      phi=0.55):
    vg = sfv.voxels.VoxelGrid(bbox, resolution=resolution)

    graph = maxflow.GraphFloat()
    nodeids = graph.add_grid_nodes(vg.shape)

    sfv.unary.add_initial_unary_costs_to_graph(images, cameras, graph, vg,
                                               gmm_samples_per_image=gmm_samples,
                                               phi=phi, weight=1)

    graph.add_grid_edges(nodeids, 0.01)
    graph.maxflow()
    seg3d = graph.get_grid_segments(vg.linear_index_grid()).astype(np.float32)

    mesh = sfv.voxels.voxel_grid_to_mesh(seg3d, bbox=bbox, crossing=0.5)

    bstart, bend = mesh['v'].min(0), mesh['v'].max(0)
    bcenter = (bend + bstart) / 2.0
    half = (bend - bstart).max()
    bstart = bcenter - half * 1.05
    bend = bcenter + half * 1.05

    return mesh, np.vstack((bstart, bend))


def estimate_volume_em(images, cameras, mesh, bbox, out_impath,
                       gmm_samples=2000,
                       resolution=200,
                       phi=0.95):
    if mesh is None:
        vg = sfv.voxels.VoxelGrid(bbox, resolution=resolution)

        graph = maxflow.GraphFloat()
        nodeids = graph.add_grid_nodes(vg.shape)

        sfv.unary.add_initial_unary_costs_to_graph(images, cameras, graph, vg,
                                                   gmm_samples_per_image=gmm_samples,
                                                   phi=phi, weight=1)

        graph.add_grid_edges(nodeids, 0.01)
        graph.maxflow()
        seg3d = graph.get_grid_segments(vg.linear_index_grid()).astype(
                np.float32)

        if seg3d.sum() == 0:
            raise NoVolumeException()

        mesh = sfv.voxels.voxel_grid_to_mesh(seg3d, bbox=bbox, crossing=0.5)
    else:
        fg_pixels = []
        bg_pixels = []
        for i in range(len(cameras)):
            P = cameras[i].projection_mat34()
            tri = mesh['v'][mesh['f'].ravel()]
            im_wh = images[0].shape[:2][::-1]
            silh = osmesa_renderer.render_silhouette(tri, P, out_wh=im_wh,
                                                     lrtb=(0, im_wh[0] - 1, 0,
                                                           im_wh[1] - 1))
            fg_pixels.append(
                    utils.pick_masked_pixels(images[i], silh, gmm_samples))
            bg_pixels.append(
                    utils.pick_masked_pixels(images[i], ~silh, gmm_samples))
        fg_pixels = np.vstack(fg_pixels)
        bg_pixels = np.vstack(bg_pixels)

        bg_model, fg_model = sfv.utils.learn_color_models(bg_pixels, fg_pixels)

        fgs = []
        for i in range(images.shape[0]):
            fg = sfv.utils.apply_color_models(images[i], bg_model, fg_model)
            fgs.append(fg)

        fig = pt.figure(figsize=(16, 12))
        sfv.utils.display_montage(fgs)
        fig.savefig(out_impath)
        fig.clear()
        pt.close()

        vg = sfv.voxels.VoxelGrid(bbox, resolution=resolution)
        graph = maxflow.GraphFloat()
        _ = graph.add_grid_nodes(vg.shape)

        volume_weight = 0.5

        unary.unary_terms_from_color_models(images, cameras, graph, vg,
                                            bg_model, fg_model, phi=phi,
                                            weight=volume_weight)

        pairwise.add_pairwise_costs_to_graph(images, cameras, graph, vg,
                                             weight=1 - volume_weight)

        graph.maxflow()
        seg3d = graph.get_grid_segments(vg.linear_index_grid()).astype(
                np.float32)

        if seg3d.sum() == 0:
            raise NoVolumeException()

        mesh = sfv.voxels.voxel_grid_to_mesh(seg3d, bbox=bbox, crossing=0.5)

    return mesh


def visualize_mesh_projections(mesh, images, cameras, out_impath):
    def display_montage(images):
        if type(images) is not list:
            images = [images[i] for i in range(images.shape[0])]

        m = sfv.utils.montage(images, 8)
        pt.imshow(m, cmap=cm.get_cmap('gray'))
        pt.clim(m.min(), m.max())
        pt.title("min: {}, max: {}".format(m.min(), m.max()))

    from dshin import osmesa_renderer
    from skimage import color as skcolor

    proj_visualizations = []
    projections = []
    for i in range(len(cameras)):
        P = cameras[i].projection_mat34()
        tri = mesh['v'][mesh['f'].ravel()]
        im_wh = images[0].shape[:2][::-1]
        silh = osmesa_renderer.render_silhouette(tri, P, out_wh=im_wh, lrtb=(
            0, im_wh[0] - 1, 0, im_wh[1] - 1))

        proj_visualizations.append(
                np.dstack((skcolor.rgb2gray(images[i]), silh,
                           np.zeros(silh.shape))))
        projections.append(silh.astype(np.bool))

    fig = pt.figure(figsize=(16, 12))
    display_montage(proj_visualizations)
    pt.tight_layout()
    fig.savefig(out_impath)

    fig.clear()
    pt.close()

    with open('{}.pickle'.format(out_impath), 'wb') as f:
        pickle.dump(projections, f, pickle.HIGHEST_PROTOCOL)


def main(images, cameras, run_id=0):
    outdir = path.expanduser('~/Dropbox/sfv_results/{}/'.format(run_id))
    if not path.isdir(outdir):
        os.makedirs(outdir)

    bbox = sfv.utils.initial_bounding_box(cameras, visualize=False)

    mesh = None

    start = time.time()
    phi = 0.55
    mesh_init, bbox = estimate_boundary(images, cameras, bbox, phi=phi)
    print("{} time elapsed: {}. phi: {}".format(
            run_id, time.time() - start, phi))
    start = time.time()
    visualize_mesh_projections(mesh_init, images, cameras,
                               path.join(outdir, '{}.png'.format(phi)))

    n_iter = 9

    try:
        for phi in [0.65, 0.70, 0.75, 0.80, 0.85, 0.90, 0.95, 0.97]:
            meshfile = path.join(outdir, 'mesh_{}.off'.format(phi))
            if path.isfile(meshfile):
                print("{} already exists.".format(meshfile))
                continue

            mesh = None

            for i in range(n_iter):
                mesh = estimate_volume_em(
                        images, cameras, mesh, bbox, phi=phi,
                        out_impath=path.join(outdir,
                                             'p_{}_{}.png'.format(i, phi)))
            print("{} time elapsed: {}. phi: {}".format(
                    run_id, time.time() - start, phi))
            start = time.time()
            visualize_mesh_projections(mesh, images, cameras,
                                       path.join(outdir, '{}.png'.format(phi)))

            with open(path.join(
                    outdir, 'mesh_{}.pickle'.format(phi)), 'wb') as f:
                pickle.dump(mesh, f, pickle.HIGHEST_PROTOCOL)
            utils.save_off_mesh(mesh, meshfile)

    except NoVolumeException:
        if mesh is None:
            return


from multiprocessing import Pool

if __name__ == '__main__':
    data = import_data.import_mat_file()
    args = []
    for k in [0,1,2,3,4,5,8,9,10,11]:
        cameras = data[k]['cameras']
        images = data[k]['images']
        args.append((images, cameras, k))

    with Pool(6) as p:
        p.starmap(main, args)
