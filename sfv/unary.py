import sfv

import sfv.voxels
from sfv import utils


def unary_terms_from_color_models(images, cameras, graph, vg, bg_model,
                                  fg_model, phi=0.55, weight=1):
    fgs = []
    for i in range(images.shape[0]):
        fg = sfv.utils.apply_color_models(images[i], bg_model, fg_model)
        fgs.append(fg)

    Gfg = vg.zero_grid()
    for i in range(len(cameras)):
        xy, ids = vg.project_to_image(cameras[i], images[i].shape)
        Gfg[vg.linear_to_multi(ids)] += fgs[i][xy[:, 1], xy[:, 0]]
    Gfg /= len(cameras)

    # vol = weight * (1 + (Gfg - phi))
    # utils.visualize_volume(vol, 40)
    # pt.figure()
    # vol = weight * (1 - (Gfg - phi))
    # utils.visualize_volume(vol, 40)


    Gfg[[0, -1], :, :] = -1
    Gfg[:, [0, -1], :] = -1
    Gfg[:, :, [0, -1]] = -1

    assert graph.get_node_count() == vg.size

    graph.add_grid_tedges(vg.linear_index_grid(), weight * (1 - (Gfg - phi)),
                          weight * (1 + (Gfg - phi)))


def add_initial_unary_costs_to_graph(images, cameras, graph, vg,
                                     gmm_samples_per_image=1200, phi=0.55,
                                     weight=1):
    bg_px = sfv.utils.initial_background_pixels(
            images, gmm_samples_per_image)
    fg_px = sfv.utils.initial_foreground_pixels(
            images, cameras, gmm_samples_per_image)
    bg_model, fg_model = sfv.utils.learn_color_models(bg_px, fg_px)

    unary_terms_from_color_models(images, cameras, graph, vg, bg_model,
                                  fg_model, phi=phi, weight=weight)
