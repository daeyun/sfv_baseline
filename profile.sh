#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"



if [ -z "$1" ]
  then
    echo "No argument supplied"
    exit 1
fi

TARGET=$(readlink -f $1)

echo "Profiling $TARGET"

set -xe

cd $DIR 

python3 -m compileall . && python3 -m cProfile -o result.profile $TARGET &&  python3 -m pstats result.profile << EOF
sort cumulative
stats 60
sort time
stats 60
EOF
