#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

set -xe

cd $DIR 

python3 -m compileall ./sfv && python3 -OO ./sfv/pipeline.py
